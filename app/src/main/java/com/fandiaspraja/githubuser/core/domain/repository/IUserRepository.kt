package com.fandiaspraja.githubuser.core.domain.repository

import com.fandiaspraja.githubuser.core.data.source.Resource
import com.fandiaspraja.githubuser.core.data.source.remote.response.ItemsItem
import com.fandiaspraja.githubuser.core.domain.model.Search
import kotlinx.coroutines.flow.Flow

interface IUserRepository {

    fun getUserByName(user: String): Flow<Resource<Search>>
}