package com.fandiaspraja.githubuser.core.data.source.remote

import android.util.Log
import com.fandiaspraja.githubuser.core.data.source.remote.network.ApiResponse
import com.fandiaspraja.githubuser.core.data.source.remote.network.ApiService
import com.fandiaspraja.githubuser.core.data.source.remote.response.ItemsItem
import com.fandiaspraja.githubuser.core.data.source.remote.response.SearchResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class RemoteDataSource(private val apiService: ApiService) {

    suspend fun getUserbyName(username : String): Flow<ApiResponse<SearchResponse>>{

        return flow {
            try {
                val response = apiService.searchUsers(username, "token fa32f95092cfbd95c689c0111da03a0294497fef")
                if (response.items.isNotEmpty()){
                    emit(ApiResponse.Success(response))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e: Exception){
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }
}