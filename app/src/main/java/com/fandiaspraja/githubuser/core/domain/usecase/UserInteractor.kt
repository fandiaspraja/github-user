package com.fandiaspraja.githubuser.core.domain.usecase

import com.fandiaspraja.githubuser.core.data.source.Resource
import com.fandiaspraja.githubuser.core.domain.model.Search
import com.fandiaspraja.githubuser.core.domain.repository.IUserRepository
import kotlinx.coroutines.flow.Flow

class UserInteractor(private val userRepository: IUserRepository): UserUseCase {

    override fun getUserByName(user: String): Flow<Resource<Search>> = userRepository.getUserByName(user)
}