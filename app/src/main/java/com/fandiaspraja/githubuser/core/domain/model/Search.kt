package com.fandiaspraja.githubuser.core.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Search(
	val totalCount: Int,
	val incompleteResults: Boolean,
	val items: List<UserGithub>
) : Parcelable