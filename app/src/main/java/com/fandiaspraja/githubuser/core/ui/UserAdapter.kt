package com.fandiaspraja.githubuser.core.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fandiaspraja.githubuser.R
import com.fandiaspraja.githubuser.core.domain.model.UserGithub
import com.fandiaspraja.githubuser.databinding.ItemSearchUserBinding
import java.util.ArrayList

class UserAdapter: RecyclerView.Adapter<UserAdapter.ListViewHolder>() {

    private var listData = ArrayList<UserGithub>()
    var onItemClick: ((UserGithub) -> Unit)? = null

    fun setData(newListData: List<UserGithub>?) {
        if (newListData == null) return
        listData.clear()
        listData.addAll(newListData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search_user, parent, false))

    override fun getItemCount() = listData.size

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val data = listData[position]
        holder.bind(data)
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemSearchUserBinding.bind(itemView)
        fun bind(data: UserGithub) {
            with(binding) {
                Glide.with(itemView.context)
                        .load(data.avatarUrl)
                        .into(imgUser)
                tvItemTitle.text = data.login
                tvItemSubtitle.text = data.type
            }
        }

        init {
            binding.root.setOnClickListener {
                onItemClick?.invoke(listData[adapterPosition])
            }
        }
    }
}