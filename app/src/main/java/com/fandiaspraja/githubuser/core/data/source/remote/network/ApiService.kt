package com.fandiaspraja.githubuser.core.data.source.remote.network

import com.fandiaspraja.githubuser.core.data.source.remote.response.DetailUserResponse
import com.fandiaspraja.githubuser.core.data.source.remote.response.SearchResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("search/users")
    suspend fun searchUsers(@Query("q") username : String, @Header("Authorization") token: String): SearchResponse

    @GET("users/{username}")
    suspend fun getDetailUser(@Path("username") username : String): DetailUserResponse

    @GET("users/{username}/followers")
    suspend fun getFollowers(@Path("username") username : String)

    @GET("users/{username}/following")
    suspend fun getFollowings(@Path("username") username : String)
}