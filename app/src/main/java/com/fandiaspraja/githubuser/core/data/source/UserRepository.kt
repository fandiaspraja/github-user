package com.fandiaspraja.githubuser.core.data.source

import com.fandiaspraja.githubuser.core.data.source.remote.RemoteDataSource
import com.fandiaspraja.githubuser.core.data.source.remote.network.ApiResponse
import com.fandiaspraja.githubuser.core.data.source.remote.response.SearchResponse
import com.fandiaspraja.githubuser.core.domain.model.Search
import com.fandiaspraja.githubuser.core.domain.repository.IUserRepository
import com.fandiaspraja.githubuser.core.utils.AppExecutors

import kotlinx.coroutines.flow.Flow

class UserRepository(
    private val remoteDataSource: RemoteDataSource,
    private val appExecutors: AppExecutors
) : IUserRepository {


    override fun getUserByName(user: String): Flow<Resource<Search>> =
        object : NetworkBoundResource<Search, SearchResponse>(){
            override fun shouldFetch(): Boolean = true

            override suspend fun createCall(): Flow<ApiResponse<SearchResponse>> =
                    remoteDataSource.getUserbyName(user)

            override suspend fun saveCallResult(data: SearchResponse) {
            }

        }.asFlow()


//    override fun getUserByName(user: String): Flow<Resource<Search>> =
//            object : NetworkBoundResource<Search, SearchResponse>(){
//
//                override fun shouldFetch(): Boolean = true
//
//                override suspend fun createCall(): Flow<ApiResponse<SearchResponse>> =
//                        remoteDataSource.getUserbyName(user)
//
//                override suspend fun saveCallResult(data: SearchResponse) {
//                }
//            }

}