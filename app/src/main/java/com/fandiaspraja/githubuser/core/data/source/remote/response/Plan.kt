package com.fandiaspraja.githubuser.core.data.source.remote.response

data class Plan(
        val privateRepos: Int,
        val name: String,
        val collaborators: Int,
        val space: Int
)
