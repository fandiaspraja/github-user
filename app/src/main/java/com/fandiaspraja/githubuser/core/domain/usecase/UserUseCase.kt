package com.fandiaspraja.githubuser.core.domain.usecase

import com.fandiaspraja.githubuser.core.data.source.Resource
import com.fandiaspraja.githubuser.core.domain.model.Search
import kotlinx.coroutines.flow.Flow

interface UserUseCase {

    fun getUserByName(user : String) : Flow<Resource<Search>>
}