package com.fandiaspraja.githubuser

import android.app.Application
import com.fandiaspraja.githubuser.core.di.networkModule
import com.fandiaspraja.githubuser.core.di.repositoryModule
import com.fandiaspraja.githubuser.di.useCaseModule
import com.fandiaspraja.githubuser.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class GithubUserApp: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@GithubUserApp)
            modules(
                listOf(
                    networkModule,
                    repositoryModule,
                    useCaseModule,
                    viewModelModule
                )
            )
        }
    }
}