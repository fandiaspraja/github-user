package com.fandiaspraja.githubuser.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fandiaspraja.githubuser.R

class DetailUserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_user)
    }
}