package com.fandiaspraja.githubuser.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.fandiaspraja.githubuser.core.data.source.Resource
import com.fandiaspraja.githubuser.core.data.source.remote.response.SearchResponse
import com.fandiaspraja.githubuser.core.domain.model.Search
import com.fandiaspraja.githubuser.core.domain.usecase.UserUseCase

class SearchViewModel(private val userUseCase: UserUseCase): ViewModel() {

    lateinit var userGit: LiveData<Resource<Search>>

    fun searchUserGithub(user: String){

        userGit = userUseCase.getUserByName(user).asLiveData()

    }
}