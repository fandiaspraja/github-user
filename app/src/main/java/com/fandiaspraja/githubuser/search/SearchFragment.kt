package com.fandiaspraja.githubuser.search

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.fandiaspraja.githubuser.core.data.source.Resource
import com.fandiaspraja.githubuser.core.ui.UserAdapter
import com.fandiaspraja.githubuser.databinding.FragmentSearchBinding
import com.fandiaspraja.githubuser.detail.DetailUserActivity
import org.koin.android.viewmodel.ext.android.viewModel

class SearchFragment : Fragment() {

    private val searchViewModel: SearchViewModel by viewModel()

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        if (activity != null){
            val userAdapter = UserAdapter()
//            userAdapter.onItemClick = { selectedData ->
//                val intent = Intent(activity, DetailUserActivity::class.java)
//                intent.putExtra(DetailUserActivity.EXTRA_DATA, selectedData)
//                startActivity(intent)
//            }



            binding.searchUser.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
                    android.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    searchViewModel.searchUserGithub(query.toString())
                    searchViewModel.userGit.observe(viewLifecycleOwner, { usergithub ->
                        Log.d("data success", "Success get data ${usergithub.data!!.items.size}")

                        if (usergithub != null) {
                            when (usergithub) {
                                is Resource.Loading -> binding.progressBar.visibility = View.VISIBLE
                                is Resource.Success -> {
                                    Log.d("success", "Success get data")
                                    binding.progressBar.visibility = View.GONE
                                    userAdapter.setData(usergithub.data!!.items)
                                }
                                is Resource.Error -> {
                                    Log.d("error", "failed get data")
                                    binding.progressBar.visibility = View.GONE
//                            binding.viewError.root.visibility = View.VISIBLE
//                            binding.viewError.tvError.text = tourism.message ?: getString(R.string.something_wrong)
                                }
                            }
                        }
                    })
                    Log.d("klik", "berhasil queri")
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })

            with(binding.rvUsersearch){
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = userAdapter
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}