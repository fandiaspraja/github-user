package com.fandiaspraja.githubuser.di

import com.fandiaspraja.githubuser.core.domain.usecase.UserInteractor
import com.fandiaspraja.githubuser.core.domain.usecase.UserUseCase
import com.fandiaspraja.githubuser.search.SearchViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModule = module {
    factory<UserUseCase> {
        UserInteractor(get())
    }
}

val viewModelModule = module {
    viewModel { SearchViewModel(get()) }
}